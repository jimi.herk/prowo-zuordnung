#import "template.typ": *
#show: project.with(
  title: "Assignment of projects",
  authors: (
    "Jimi Herken",
    "Friedrich Darius",
  ),
  date: "May 18, 2023",
)

= Objective
We want the students to be as happy as possible with the project they have been assigned. To do so, every student can state their preference by picking 3 projects as first, second and third picks. Additionally, they can state what project they explicitly do not want to take part in. So, our objective is to assign each student a project that will contribute to maximizing the overall happiness score.

= Mathematical implementation
The students pick are represented by a matrix $c$, where every student (index $i$) gives each project (index $j$) a value rating of:
- 3 (most preferred)
- 2 (second-most preferred)
- 1 (third-most preferred)
- 0 (no preference stated) or 
- -10 (explicitly not preferred)
For example: Student i picked project j as their third-most preferred project, so $c_(i j) = 1$
\ \
All possible assignments are stored in a binary matrix $x$ where $1$ represents the student $i$ being in the project $j$, while $0$ means the student is not in the project.
\ \
The variables $n$ and $m$ represent the number of students and the number of projects respectively.

== Constraints
One must take into account that, of course, a student can only be in one project at once, so
$ sum_(j=0)^m x_(i j) = 1 forall i $
Additionally, the number of slots in a project is limited - otherwise we could just stuff everyone in their preferred project. With $s$ being a vector that stores the maximum capacity for each project $j$, we can represent this constraint as
$ sum_(i=0)^n x_(i j) lt.eq s_j forall j $

== Value function
Our goal is it to maximise the value function
$ v(x, c) = sum_(i=0)^n sum_(j=0)^m x_(i j) dot c_(i j) $

= Python implementation
We are using Google's OR Tools to optimize the value function using linear optimization.
Take a look at the code #link("https://git.kkg.berlin/jimi.herk/prowo-zuordnung")[here]